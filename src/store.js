import Vue from 'vue'
import Vuex from 'vuex'
import projects from './store/modules/projects'
import contracts from './store/modules/contracts'
import uiState from './store/modules/ui-state'
import resources from './store/modules/resources'
import activities from './store/modules/activities'
import users from './store/modules/users'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    contracts,
    projects,
    uiState,
    resources,
    activities,
    users
  }
})
