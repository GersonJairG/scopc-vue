import colombiaData from './data/colombia-data.json'

const CONSTANTS = {
  // API: 'http://localhost/Aprendizaje/scopc-back/public/api', local
  // API: 'http://localhost:8000/api', brigitte
  API: 'https://scopc-back-heroku.herokuapp.com/api', // heroku
  COLOMBIA_DATA: colombiaData
}

export default CONSTANTS
