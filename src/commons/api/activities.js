import CONSTANTS from '../constants'
import axios from 'axios'
import { formatingDate } from '../../commons/formatDate'

const activities = {
  getTypes () {
    return axios.get(`${CONSTANTS.API}/tipos-actividad`)
  },
  getStates () {
    return axios.get(`${CONSTANTS.API}/estados-actividad`)
  },
  store (parameters) {  
    return axios.post(`${CONSTANTS.API}/actividades/new`, {
      nombre: parameters.nombre,
      unidad: parameters.unidad,
      precio: parameters.precio,
      fechaCreacion: formatingDate(new Date()),      
      fechaEntrega: parameters.fechaEntrega,
      fechaActualizacion: formatingDate(new Date()),
      idTipoActividad: parameters.tipoActividad,
      idProyecto: parameters.idProject,
      idEstadoActividad: parameters.estadoActividad

    })
  },
  update (id, data) {
    return axios.patch(`${CONSTANTS.API}/actividades/${id}`, data)
  }
}

export default activities
