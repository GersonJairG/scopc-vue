import CONSTANTS from '../constants'
import axios from 'axios'

const resources = {
  list () {
    return Promise.all([
      axios.get(`${CONSTANTS.API}/recursos-mano-obra`),
      axios.get(`${CONSTANTS.API}/recursos-material`),
      axios.get(`${CONSTANTS.API}/recursos-maquinaria`)
    ])
  }
}

export default resources
