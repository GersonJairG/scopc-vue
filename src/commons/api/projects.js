import CONSTANTS from '../constants'
import axios from 'axios'

const projects = {
  list (id) {
    return axios.get(`${CONSTANTS.API}/proyectos/byProjects/${id}`)
  },
  get (id) {
    return Promise.all([
      axios.get(`${CONSTANTS.API}/proyectos/${id}`),
      axios.get(`${CONSTANTS.API}/proyectos/getActivities/${id}`)
    ])
  },
  update (id, data) {
    return axios.patch(`${CONSTANTS.API}/proyectos/${id}`, data)
  },
  remove (id) {
    return axios.delete(`${CONSTANTS.API}/proyectos/${id}`)
  },
  store (parameters) {
    return axios.post(`${CONSTANTS.API}/proyectos/new`, {
      nombre: parameters.nombre,
      descripcion: parameters.descripcion,
      fechaInicio: parameters.fechaInicio,
      departamento: parameters.departamento,
      ciudad: parameters.ciudad,
      idContrato: parameters.tipoContrato,
      idUsuario: parameters.director
    })
  }
}

export default projects
