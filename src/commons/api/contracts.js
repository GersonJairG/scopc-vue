import CONSTANTS from '../constants'
import axios from 'axios'

const contracts = {
  get () {
    return axios.get(`${CONSTANTS.API}/contratos`)
  }
}

export default contracts
