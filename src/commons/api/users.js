import CONSTANTS from '../constants'
import axios from 'axios'

const users = {
  list () {
    return axios.get(`${CONSTANTS.API}/usuarios`)
  }
}

export default users
