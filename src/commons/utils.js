import CONSTANTS from './constants'

const colombiaData = CONSTANTS.COLOMBIA_DATA

/** Funcion que retorna un array de ciudades de Colombia dado un dpto */
export function getCities (departamentName) {
  const departamentInfo = colombiaData.filter((departament) => departamentName === departament.departamento)
  if (departamentInfo.length) {
    return departamentInfo[0].ciudades
  }
  return []
}

/** Funcion que retorna un array de departamentos de Colombia */
export function getDepartaments () {
  return colombiaData.reduce((myArr, data) => {
    myArr.push(data.departamento)
    return myArr
  }, [])
}
