
// Funcion para formatear la fecha y sea aceptada por SQL:
export function formatingDate (myDate) {
  var año = myDate.getFullYear();
  var mes = myDate.getMonth()+1;
  
  var dia = '';
  if(myDate.getDate()<=10){
    dia = '0'+ myDate.getDate();
  }else{
    dia = myDate.getDate();
  }
  var horas = myDate.getHours();
  var minutos = myDate.getMinutes();
  var segundos = myDate.getSeconds();

  // return myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate() +
  //   ' ' + myDate.getHours() + ':' + myDate.getMinutes() + ':' + myDate.getSeconds()

  return año+ '-' + mes + '-' + dia + ' ' + horas + ':' + minutos+ ':' + segundos;
}
