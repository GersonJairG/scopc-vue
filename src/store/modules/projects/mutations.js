import * as types from '../../mutation-types'

const mutations = {
  [types.SET_PROJECTS] (state, payload) {
    state.projects = payload
  },
  [types.SET_ACTIVE_NEW_PROJECT] (state, payload) {
    state.activeNewProject = payload
  },
  [types.SET_ACTIVE_EDIT_PROJECT] (state, payload) {
    state.activeEditProject = payload
  }
}

export default mutations
