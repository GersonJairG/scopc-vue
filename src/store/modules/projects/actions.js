import * as types from '../../mutation-types'
import projects from '../../../commons/api/projects'

const list = ({ dispatch, commit, state }, payload) => {
  const user = JSON.parse(localStorage.getItem('user'))
  projects.list(user.id)
    .then(projects => {
      commit(types.SET_PROJECTS, projects.data)
    })
    .catch(error => {
      console.log(error)
    })
}

const update = ({ dispatch, commit, state }, payload) => {
  return projects.update(payload.id, payload.data)
}

const get = ({ dispatch, commit, state }, payload) => {
  return projects.get(payload.id)
}

const remove = ({ dispatch, commit, state }, payload) => {
  return projects.remove(payload.id)
}

const store = ({ dispatch, commit, state }, newProject) => {
  return projects.store(newProject)
}

const setActiveNewProject = ({ dispatch, commit, state }, payload) => {
  commit(types.SET_ACTIVE_NEW_PROJECT, payload)
}

const setActiveEditProject = ({ dispatch, commit, state }, payload) => {
  commit(types.SET_ACTIVE_EDIT_PROJECT, payload)
}

export default {
  list,
  update,
  get,
  remove,
  store,
  setActiveNewProject,
  setActiveEditProject
}
