const getters = {
  getProjects: state => state.projects,
  getActiveNewProject: state => state.activeNewProject,
  getActiveEditProject: state => state.activeEditProject,
  getProjectsWithLinks: state => {
    const projects = []
    state.projects.forEach(project => {
      projects.push({ ...project, link: `/project/${project.id}` })
    })
    return projects
  }
}

export default getters
