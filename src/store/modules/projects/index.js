import getters from './getters'
import actions from './actions'
import mutations from './mutations'

const state = {
  projects: [],
  activeNewProject: false,
  activeEditProject: false,
  currentProject: null
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
