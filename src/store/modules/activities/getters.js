const getters = {
  types: state => state.types,
  states: state => state.states
}

export default getters
