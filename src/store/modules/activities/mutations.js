import * as types from '../../mutation-types'

const mutations = {
  [types.SET_ACTIVITY_TYPES] (state, payload) {
    state.types = payload
  },
  [types.SET_ACTIVITY_STATES] (state, payload) {
    state.states = payload
  }
}

export default mutations
