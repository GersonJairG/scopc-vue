import * as types from '../../mutation-types'
import activities from '../../../commons/api/activities'

const activityTypes = ({ commit }) => {
  activities.getTypes()
    .then(response => {
      commit(types.SET_ACTIVITY_TYPES, response.data)
    })
    .catch(error => {
      console.log(error)
    })
}

const activityStates = ({ commit }) => {
  activities.getStates()
    .then(response => {
      commit(types.SET_ACTIVITY_STATES, response.data)
    })
    .catch(error => {
      console.log(error)
    })
}

const store = ({ dispatch, commit, state }, newActivity) => {
  return activities.store(newActivity)
}

const update = ({ dispatch, commit, state }, payload) => {
  return activities.update(payload.id, payload.data)
}

export default {
  activityTypes,
  activityStates,
  store,
  update
}
