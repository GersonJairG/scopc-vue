import * as types from '../../mutation-types'

const mutations = {
  [types.SET_PROJECT_MANAGERS] (state, payload) {
    state.directors = payload
  }
}

export default mutations
