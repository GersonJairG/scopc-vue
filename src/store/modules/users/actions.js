import * as types from '../../mutation-types'
import users from '../../../commons/api/users'

const list = ({ dispatch, commit, state }) => {
  users.list()
    .then(response => {
      commit(types.SET_PROJECT_MANAGERS, response.data)
    })
    .catch(error => {
      console.log(error)
    })
}

export default {
  list
}
