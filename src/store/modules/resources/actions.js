import * as types from '../../mutation-types'
import resources from '../../../commons/api/resources'

const list = ({ commit }, payload) => {
  resources.list()
    .then(resourcesList => {
      const humans = resourcesList[0].data
      const materials = resourcesList[1].data
      const machines = resourcesList[2].data
      commit(types.GET_RESOURCES_DATA, { humans, machines, materials })
    })
    .catch(error => {
      console.log(error)
    })
}

export default {
  list
}
