import resourceTypes from '../../../commons/data/resources-type'

const getters = {
  humansResources: state => (
    state.humans.reduce((arr, value) => {
      const humansTypes = resourceTypes.humans
      const newValue = Object.assign({}, value)
      newValue.type = humansTypes[value.id_tipo_mano_de_obra - 1].name
      arr.push(newValue)
      return arr
    }, [])
  ),
  machinesResources: state => (
    state.machines.reduce((arr, value) => {
      const machineTypes = resourceTypes.machines
      const newValue = Object.assign({}, value)
      newValue.type = machineTypes[value.id_tipo_maquinaria - 1].name
      arr.push(newValue)
      return arr
    }, [])
  ),
  materialsResources: state => (
    state.materials.reduce((arr, value) => {
      const materialsTypes = resourceTypes.materials
      const newValue = Object.assign({}, value)
      newValue.type = materialsTypes[value.id_tipo_material - 1].name
      arr.push(newValue)
      return arr
    }, [])
  )
}

export default getters
