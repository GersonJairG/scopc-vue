import * as types from '../../mutation-types'

const mutations = {
  [types.GET_RESOURCES_DATA] (state, payload) {
    state.humans = payload.humans
    state.machines = payload.machines
    state.materials = payload.materials
  }
}

export default mutations
