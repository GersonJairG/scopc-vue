import * as types from '../../mutation-types'

const mutations = {
  [types.SET_PROJECT_ERROR_MSG] (state, payload) {
    state.errorProjectModule = payload
  },
  [types.SET_ACTIVE_MODAL] (state, payload) {
    state.activeModal = payload
  }
}

export default mutations
