import * as types from '../../mutation-types'

const setActiveModal = ({ commit }, payload) => {
  commit(types.SET_ACTIVE_MODAL, payload)
}

export default {
  setActiveModal
}
