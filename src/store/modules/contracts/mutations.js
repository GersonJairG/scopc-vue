import * as types from '../../mutation-types'

const mutations = {
  [types.SET_CONTRACTS] (state, payload) {
    state.contracts = payload
  }
}

export default mutations
