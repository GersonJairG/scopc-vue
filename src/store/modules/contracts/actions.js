import * as types from '../../mutation-types'
import contracts from '../../../commons/api/contracts'

const get = ({ dispatch, commit, state }, payload) => {
  contracts.get()
    .then(contracts => {
      // console.log(contracts.data)
      commit(types.SET_CONTRACTS, contracts.data)
    })
    .catch(error => {
      console.log(error)
    })
}

export default {
  get
}
